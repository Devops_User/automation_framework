package generic;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Properties;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

public class Utility
{
	public static String ReadCell(String FilePath, String Sheet, String Column, int Row) throws IOException
	{
		File myFile = new File(FilePath); 
		FileInputStream fis = new FileInputStream(myFile); // Finds the workbook instance for XLSX file 
		XSSFWorkbook myWorkBook = new XSSFWorkbook (fis); // Return first sheet from the XLSX workbook 
		XSSFSheet mySheet = myWorkBook.getSheet(Sheet);
		Iterator<Row> rowIterator = mySheet.rowIterator();
        Row headerRow = (Row) rowIterator.next();
        int numberOfCells = headerRow.getPhysicalNumberOfCells();
        XSSFRow row = mySheet.getRow(0);
        
        int cellnum = 0;
        for(int i = 0;i<numberOfCells;i++)
        {
        	XSSFCell cell = row.getCell(i);
        	String cellVal = cell.getStringCellValue();
        	if(cellVal.equals(Column))
        	{
        		cellnum = i; 
        	}
        }
        XSSFRow CurrentRow = mySheet.getRow(Row);
        return CurrentRow.getCell(cellnum).getStringCellValue();
	}
	
	public static int GetRowCount(String FilePath, String Sheet) throws IOException
	{
		File myFile = new File(FilePath); 
		FileInputStream fis = new FileInputStream(myFile); // Finds the workbook instance for XLSX file 
		XSSFWorkbook myWorkBook = new XSSFWorkbook (fis); // Return first sheet from the XLSX workbook 
		XSSFSheet mySheet = myWorkBook.getSheet(Sheet);
		return mySheet.getLastRowNum();
	}
	
	public static String ReadCellUsingText(String File, String Sheet, String Text, String Column) throws IOException
	{
		String value = null;
		File myFile = new File(File); 
		FileInputStream fis = new FileInputStream(myFile); // Finds the workbook instance for XLSX file 
		XSSFWorkbook myWorkBook = new XSSFWorkbook (fis); // Return first sheet from the XLSX workbook 
		XSSFSheet mySheet = myWorkBook.getSheet(Sheet);
		Iterator<Row> rowIterator = mySheet.rowIterator();
        Row headerRow = (Row) rowIterator.next();
        int numberOfCells = headerRow.getPhysicalNumberOfCells();
        XSSFRow row = mySheet.getRow(0);
        
        int cellnum = 0;
        for(int i = 0;i<numberOfCells;i++)
        {
        	XSSFCell cell = row.getCell(i);
        	String cellVal = cell.getStringCellValue();
        	if(cellVal.equals(Column))
        	{
        		cellnum = i; 
        	}
        }
        int rows = mySheet.getLastRowNum();
        for(int i = 1;i<rows+1;i++)
        {
        	XSSFRow currentRow = mySheet.getRow(i);
        	XSSFCell cell = currentRow.getCell(0);
        	String cellVal = cell.getStringCellValue();
        	if(cellVal.equals(Text))
        	{
            	XSSFCell Cell = currentRow.getCell(cellnum);
            	value = Cell.getStringCellValue();
        	}
        } 
		return value;
	}

	public static void WriteCell(String FilePath, String Sheet, String Column, int Row, String value) throws IOException
	{
		File myFile = new File(FilePath); 
		FileInputStream fis = new FileInputStream(myFile); // Finds the workbook instance for XLSX file 
		XSSFWorkbook myWorkBook = new XSSFWorkbook (fis); // Return first sheet from the XLSX workbook 
		XSSFSheet mySheet = myWorkBook.getSheet(Sheet);
		Iterator<Row> rowIterator = mySheet.rowIterator();
	    Row headerRow = (Row) rowIterator.next();
	    int numberOfCells = headerRow.getPhysicalNumberOfCells();
	    XSSFRow row = mySheet.getRow(0);
	    
	    int cellnum = 0;
	    for(int i = 0;i<numberOfCells;i++)
	    {
	    	XSSFCell cell = row.getCell(i);
	    	String cellVal = cell.getStringCellValue();
	    	if(cellVal.equals(Column))
	    	{
	    		cellnum = i; 
	    	}
	    }
	    
	    XSSFRow CurrentRow = mySheet.getRow(Row);
//	    String x = CurrentRow.getCell(cellnum).getStringCellValue();

	    CurrentRow.getCell(cellnum).setCellValue(value);
	    
        FileOutputStream fileOut = new FileOutputStream(myFile);  
        myWorkBook.write(fileOut);  
        fileOut.flush();
        fileOut.close(); 
	}
	
	public static int GetRowUsingText(String File, String Sheet, String Text, String Column) throws IOException
	{
		int value = 0;
		File myFile = new File(File); 
		FileInputStream fis = new FileInputStream(myFile); // Finds the workbook instance for XLSX file 
		XSSFWorkbook myWorkBook = new XSSFWorkbook (fis); // Return first sheet from the XLSX workbook 
		XSSFSheet mySheet = myWorkBook.getSheet(Sheet);
		Iterator<Row> rowIterator = mySheet.rowIterator();
        Row headerRow = (Row) rowIterator.next();
        int numberOfCells = headerRow.getPhysicalNumberOfCells();
        XSSFRow row = mySheet.getRow(0);
        
        int cellnum = 0;
        for(int i = 0;i<numberOfCells;i++)
        {
        	XSSFCell cell = row.getCell(i);
        	String cellVal = cell.getStringCellValue();
        	if(cellVal.equals(Column))
        	{
        		cellnum = i; 
        	}
        }
        int rows = mySheet.getLastRowNum();
        for(int i = 1;i<rows+1;i++)
        {
        	XSSFRow currentRow = mySheet.getRow(i);
        	XSSFCell cell = currentRow.getCell(0);
        	String cellVal = cell.getStringCellValue();
        	if(cellVal.equals(Text))
        	{
        		value = i;
        		break;
        	}
        } 
		return value;
	}
	
	public static String captureScreenshot(WebDriver driver) throws IOException
	{
		TakesScreenshot newScreen = (TakesScreenshot) driver;
	    String scnShot = newScreen.getScreenshotAs(OutputType.BASE64);
	    return "data:image/png;base64, " + scnShot ;
	}

	public static String getPropertyValue(String filePath,String key) throws FileNotFoundException, IOException
	{
		String value = "";
		Properties ppt = new Properties();
		try{
		ppt.load(new FileInputStream(filePath));
		value = ppt.getProperty(key);
		}
		catch(Exception e)
		{
		}
		return value;
	}
	
	public static String getElementDescription(String key) throws FileNotFoundException, IOException
	{
		String value = getPropertyValue(Base.ElementsDescription,key);
		return value;
	}
	
	public static String getValue(String key,LinkedHashMap<String,Object> TestInfo) throws FileNotFoundException, IOException
	{
		int Row = 0;
    	String PropertyFilePath = (String) TestInfo.get("PropertyFilePath");
    	String DataFilePath = (String) TestInfo.get("DataFilePath");
    	String TestName = (String) TestInfo.get("TestName");
    	try
    	{
    		Row = Integer.parseInt((String) TestInfo.get("ExecuteRow")); //((int) TestInfo.get("ExecuteRows"));
    	}
    	catch(Exception e)
    	{
    		Row = (Integer) (TestInfo.get("ExecuteRow"));
    	}
    	
		String value = "";
		Properties ppt = new Properties();
		ppt.load(new FileInputStream(PropertyFilePath));
		String valueTemp = ppt.getProperty(key);
		if(valueTemp.startsWith("DT_"))
		{
//			int Row = ((int) TestInfo.get("ExecuteRow"));
			value = Utility.ReadCell(DataFilePath, TestName, valueTemp, Row);
		}
		else
		{
			value = valueTemp;
		}
		return value;
	}
	
	public static void contains(String mainString,String subString,LinkedHashMap<String,Object> TestInfo) throws IOException
	{
		WebDriver driver = (WebDriver) TestInfo.get("driver");
		ExtentTest ExtentTest = (ExtentTest) TestInfo.get("ExtentTest");
		if(mainString.contains(subString))
		{
			String path = captureScreenshot(driver);
			ExtentTest.log(LogStatus.PASS, mainString + ": contains : " + subString + ExtentTest.addBase64ScreenShot(path));
		}
		else
		{
			String path = captureScreenshot(driver);
			ExtentTest.log(LogStatus.FAIL, mainString + " : contains : " + subString + ExtentTest.addBase64ScreenShot(path));
		}
	}

}