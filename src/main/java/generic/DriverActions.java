package generic;

import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.List;

import org.apache.commons.codec.binary.Base64;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

public class DriverActions 
{
	
	public static void captureScreenshot(LinkedHashMap<String,Object> TestInfo) throws InterruptedException, IOException
	{
		ExtentTest ExtentTest = (ExtentTest) (TestInfo.get("ExtentTest"));
		WebDriver driver = (WebDriver) (TestInfo.get("driver"));
		String path = null;
		path = Utility.captureScreenshot(driver);
		ExtentTest.log(LogStatus.INFO, "Screenshot" + ExtentTest.addBase64ScreenShot(path));
	}
	
	public static void navigate(LinkedHashMap<String,Object> TestInfo, String url, String screenshot) throws InterruptedException, IOException
	{
		ExtentTest ExtentTest = (ExtentTest) (TestInfo.get("ExtentTest"));
		WebDriver driver = (WebDriver) (TestInfo.get("driver"));
		String path = null;
		driver.get(url);	
		Thread.sleep(500);
		if(screenshot.equalsIgnoreCase("1"))
		{
			path = Utility.captureScreenshot(driver);
			ExtentTest.log(LogStatus.INFO, "Browser has been launched, url is : " + url + ExtentTest.addBase64ScreenShot(path));
		}
		else
		{
			ExtentTest.log(LogStatus.INFO, "Browser has been launched, url is : " + url);
		}
	}
	
	public static void click(WebElement element, LinkedHashMap<String,Object> TestInfo, String ElementDesc, String screenshot) throws IOException, InterruptedException
	{		
		ExtentTest ExtentTest = (ExtentTest) (TestInfo.get("ExtentTest"));
		WebDriver driver = (WebDriver) (TestInfo.get("driver"));
		String path = null;
		WebDriverWait wait = new WebDriverWait(driver, 120);
		wait.until(ExpectedConditions.visibilityOf(element));
		wait.until(ExpectedConditions.elementToBeClickable(element));
		element.click();
//		moveToElement(element,TestInfo);
		Thread.sleep(500);
		if(screenshot.equalsIgnoreCase("1"))
		{
			path = Utility.captureScreenshot(driver);
			ExtentTest.log(LogStatus.INFO, ElementDesc +" has been clicked" +  ExtentTest.addBase64ScreenShot(path));
		}
		else
		{
			ExtentTest.log(LogStatus.INFO, ElementDesc +" has been clicked");
		}
	}
	
	public static boolean softClick(WebElement element, LinkedHashMap<String,Object> TestInfo, String ElementDesc, String screenshot) throws IOException
	{		
		ExtentTest ExtentTest = (ExtentTest) (TestInfo.get("ExtentTest"));
		WebDriver driver = (WebDriver) (TestInfo.get("driver"));
		String path = null;
		WebDriverWait wait = new WebDriverWait(driver, 120);
		try
		{
			wait.until(ExpectedConditions.visibilityOf(element));
			wait.until(ExpectedConditions.elementToBeClickable(element));
//			moveToElement(element,TestInfo);
			element.click();
			Thread.sleep(500);
			if(screenshot.equalsIgnoreCase("1"))
			{
				path = Utility.captureScreenshot(driver);
				ExtentTest.log(LogStatus.INFO, ElementDesc +" has been clicked" +  ExtentTest.addBase64ScreenShot(path));
			}
			else
			{
				ExtentTest.log(LogStatus.INFO, ElementDesc +" has been clicked");
			}
			return true;
		}
		catch(Exception e)
		{
			path = Utility.captureScreenshot(driver);
			ExtentTest.log(LogStatus.WARNING, ElementDesc + " : error :: " + e.getMessage() + ExtentTest.addBase64ScreenShot(path));
			return false;
		}
	}
	
	public static void clear(WebElement element, LinkedHashMap<String,Object> TestInfo, String ElementDesc, String screenshot) throws IOException, InterruptedException
	{		
		ExtentTest ExtentTest = (ExtentTest) (TestInfo.get("ExtentTest"));
		WebDriver driver = (WebDriver) (TestInfo.get("driver"));
		String path = null;
		WebDriverWait wait = new WebDriverWait(driver, 120);
		wait.until(ExpectedConditions.visibilityOf(element));
		wait.until(ExpectedConditions.elementToBeClickable(element));
		moveToElement(element,TestInfo);
		element.clear();
		Thread.sleep(500);
		if(screenshot.equalsIgnoreCase("1"))
		{
			path = Utility.captureScreenshot(driver);
			ExtentTest.log(LogStatus.INFO, ElementDesc +" has been clicked" +  ExtentTest.addBase64ScreenShot(path));
		}
		else
		{
			ExtentTest.log(LogStatus.INFO, ElementDesc +" has been clicked");
		}
	}
	
	public static void sendKeys(WebElement element,String text, LinkedHashMap<String,Object> TestInfo, String ElementDesc, String screenshot) throws InterruptedException, IOException
	{		
		ExtentTest ExtentTest = (ExtentTest) (TestInfo.get("ExtentTest"));
		WebDriver driver = (WebDriver) (TestInfo.get("driver"));
		String path = null;
		WebDriverWait wait = new WebDriverWait(driver, 120);
		wait.until(ExpectedConditions.visibilityOf(element));
		moveToElement(element,TestInfo);
		element.sendKeys(text);
		Thread.sleep(200);
		if(screenshot.equalsIgnoreCase("1"))
		{
			path = Utility.captureScreenshot(driver);
			ExtentTest.log(LogStatus.INFO, text.toString() +" has been set in " + ElementDesc + ExtentTest.addBase64ScreenShot(path));
		}
		else
		{
			ExtentTest.log(LogStatus.INFO, text.toString() +" has been set in " + ElementDesc);
		}
	}
	
	public static void sendKeys_Password(WebElement element,String text, LinkedHashMap<String,Object> TestInfo, String ElementDesc, String screenshot) throws InterruptedException, IOException
	{		
		ExtentTest ExtentTest = (ExtentTest) (TestInfo.get("ExtentTest"));
		WebDriver driver = (WebDriver) (TestInfo.get("driver"));
		String path = null;
		WebDriverWait wait = new WebDriverWait(driver, 120);
		wait.until(ExpectedConditions.visibilityOf(element));
		moveToElement(element,TestInfo);
		element.sendKeys(text);
		Thread.sleep(200);
		
		
		byte[] encodedText = Base64.encodeBase64(text.getBytes());
//		System.out.println("encodedBytes "+ new String(encodedBytes));
		
		if(screenshot.equalsIgnoreCase("1"))
		{
			path = Utility.captureScreenshot(driver);
			ExtentTest.log(LogStatus.INFO, "Password has been set in " + ElementDesc + ExtentTest.addBase64ScreenShot(path));
		}
		else
		{
			ExtentTest.log(LogStatus.INFO, "Password has been set in " + ElementDesc);
		}
	}
	
	public static void softSendKeys(WebElement element,String text, LinkedHashMap<String,Object> TestInfo, String ElementDesc, String screenshot)
	{		
		ExtentTest ExtentTest = (ExtentTest) (TestInfo.get("ExtentTest"));
		WebDriver driver = (WebDriver) (TestInfo.get("driver"));
		String path = null;
		WebDriverWait wait = new WebDriverWait(driver, 120);
		try
		{
			wait.until(ExpectedConditions.visibilityOf(element));
			moveToElement(element,TestInfo);
			element.sendKeys(text);
			Thread.sleep(200);
			if(screenshot.equalsIgnoreCase("1"))
			{
				path = Utility.captureScreenshot(driver);
				ExtentTest.log(LogStatus.INFO, text.toString() +" has been set in " + ElementDesc + ExtentTest.addBase64ScreenShot(path));
			}
			else
			{
				ExtentTest.log(LogStatus.INFO, text.toString() +" has been set in " + ElementDesc);
			}
		}
		catch(Exception e)
		{
			try 
			{
				path = Utility.captureScreenshot(driver);
				ExtentTest.log(LogStatus.WARNING, ElementDesc + " : error :: " + e.getMessage() + ExtentTest.addBase64ScreenShot(path));
			} 
			catch (IOException e1) 
			{
				
			}
		}
	}
	
	public static String getText(WebElement element, LinkedHashMap<String,Object> TestInfo, String ElementDesc)
	{
		ExtentTest ExtentTest = (ExtentTest) (TestInfo.get("ExtentTest"));
		WebDriver driver = (WebDriver) (TestInfo.get("driver"));
		String path = null;
		try
		{
			element.getText();
		}
		catch(Exception e)
		{
			try 
			{
				path = Utility.captureScreenshot(driver);
				ExtentTest.log(LogStatus.FAIL, ElementDesc + " : error :: " + e.getMessage() + ExtentTest.addBase64ScreenShot(path));
			} 
			catch (IOException e1) 
			{
				
			}
		}
		return element.getText();
	}
	
	public static boolean isDisplayed(WebElement element, LinkedHashMap<String,Object> TestInfo, String ElementDesc, String screenshot)
	{
		ExtentTest ExtentTest = (ExtentTest) (TestInfo.get("ExtentTest"));
		WebDriver driver = (WebDriver) (TestInfo.get("driver"));
		String path = null;
		try
		{
			moveToElement(element,TestInfo);
			element.isDisplayed();
			if(screenshot.equalsIgnoreCase("1"))
			{
			path = Utility.captureScreenshot(driver);
			ExtentTest.log(LogStatus.PASS, ElementDesc +" is displayed" + ExtentTest.addBase64ScreenShot(path));
			}
			else
			{
				ExtentTest.log(LogStatus.PASS, ElementDesc +" is displayed");
			}
		}
		catch(Exception e)
		{
			try 
			{
				path = Utility.captureScreenshot(driver);
			} 
			catch (IOException e1) 
			{
				
			}
			ExtentTest.log(LogStatus.FAIL, ElementDesc + " : error :: " + e.getMessage() + ExtentTest.addBase64ScreenShot(path));
		}
		return false;
	}
	
	public static void validateDropdownOptions(WebElement element,String values, LinkedHashMap<String,Object> TestInfo, String ElementDesc)
	{
    	ExtentTest test = (ExtentTest) TestInfo.get("ExtentTest");
    	Select dropdown = new Select(element);
    	List<WebElement> list = dropdown.getOptions();
    	String[] Values = values.split("::");
    	LinkedHashMap<String,String> options = new LinkedHashMap<String,String>();
    	
		for(WebElement option:list)
		{
			String text = option.getText();
			options.put(text, text);
		}
		
    	for(String value:Values)
    	{
    		String path;
			if(options.containsKey(value))
    		{
    			test.log(LogStatus.PASS, value +" is one of the options in "+ElementDesc);
    		}
			else
			{
				test.log(LogStatus.FAIL, value +" is not one of the options in "+ElementDesc);
    		}
    	}
	}

	public static void selectDropdownOption(WebElement element,String value, LinkedHashMap<String,Object> TestInfo, String ElementDesc, String screenshot) throws IOException, InterruptedException
	{
		ExtentTest ExtentTest = (ExtentTest) (TestInfo.get("ExtentTest"));
		WebDriver driver = (WebDriver) (TestInfo.get("driver"));
    	String path = null;
    	Select dropdown = new Select(element);
    	//List<WebElement> list = dropdown.getOptions();
    	//String[] Values = values.split("::");
//    	dropdown.selectByValue(value);
    	
    	moveToElement(element,TestInfo);
    	dropdown.selectByVisibleText(value);
		Thread.sleep(500);
		if(screenshot.equalsIgnoreCase("1"))
		{
			path = Utility.captureScreenshot(driver);
			ExtentTest.log(LogStatus.INFO, ElementDesc +" has been clicked" +  ExtentTest.addBase64ScreenShot(path));
		}
		else
		{
			ExtentTest.log(LogStatus.INFO, ElementDesc +" has been clicked");
		}
	}

	public static void pageDown(LinkedHashMap<String,Object> TestInfo,String screenshot) throws IOException, InterruptedException
	{
		ExtentTest ExtentTest = (ExtentTest) (TestInfo.get("ExtentTest"));
		WebDriver driver = (WebDriver) (TestInfo.get("driver"));
    	String path = null;
		Thread.sleep(500);
		
		Actions action = new Actions(driver);
    	action.sendKeys(Keys.PAGE_DOWN).perform();    	
    	Thread.sleep(2000);
    	
		if(screenshot.equalsIgnoreCase("1"))
		{
			path = Utility.captureScreenshot(driver);
			ExtentTest.log(LogStatus.INFO, "Page down has been performed." +  ExtentTest.addBase64ScreenShot(path));
		}
		else
		{
			ExtentTest.log(LogStatus.INFO, "Page down has been performed.");
		}
	}
	
	
	public static void pageUp(LinkedHashMap<String,Object> TestInfo,String screenshot) throws IOException, InterruptedException
	{
		ExtentTest ExtentTest = (ExtentTest) (TestInfo.get("ExtentTest"));
		WebDriver driver = (WebDriver) (TestInfo.get("driver"));
    	String path = null;
		Thread.sleep(500);
		
		Actions action = new Actions(driver);
    	action.sendKeys(Keys.PAGE_UP).perform();    	
    	Thread.sleep(2000);
    	
		if(screenshot.equalsIgnoreCase("1"))
		{
			path = Utility.captureScreenshot(driver);
			ExtentTest.log(LogStatus.INFO, "Page up has been performed." +  ExtentTest.addBase64ScreenShot(path));
		}
		else
		{
			ExtentTest.log(LogStatus.INFO, "Page up has been performed.");
		}
	}
	
	public static void hoverOverElement(WebElement element,LinkedHashMap<String,Object> TestInfo, String ElementDesc,String screenshot) throws IOException, InterruptedException
	{
		ExtentTest ExtentTest = (ExtentTest) (TestInfo.get("ExtentTest"));
		WebDriver driver = (WebDriver) (TestInfo.get("driver"));
    	String path = null;
		Thread.sleep(500);
		
		Actions action = new Actions(driver);
    	action.moveToElement(element).perform();    	
    	Thread.sleep(500);
    	
		if(screenshot.equalsIgnoreCase("1"))
		{
			path = Utility.captureScreenshot(driver);
			ExtentTest.log(LogStatus.INFO, "Hovered over element : "+ElementDesc +  ExtentTest.addBase64ScreenShot(path));
		}
		else
		{
			ExtentTest.log(LogStatus.INFO, "Hovered over element : "+ElementDesc );
		}
	}
	
	
	
	public static void dragAndDropBasedOnOffset(WebElement sourceElement,String sourceElementDesc,int x,int y, LinkedHashMap<String,Object> TestInfo,String screenshot) throws IOException, InterruptedException
	{
		ExtentTest ExtentTest = (ExtentTest) (TestInfo.get("ExtentTest"));
		WebDriver driver = (WebDriver) (TestInfo.get("driver"));
    	String path = null;
		Thread.sleep(500);
		
		Actions action = new Actions(driver);
		action.dragAndDropBy(sourceElement, x, y).build().perform();
		Thread.sleep(1000);
    	
		if(screenshot.equalsIgnoreCase("1"))
		{
			path = Utility.captureScreenshot(driver);
			ExtentTest.log(LogStatus.INFO, sourceElementDesc +" has been moved." +  ExtentTest.addBase64ScreenShot(path));
		}
		else
		{
			ExtentTest.log(LogStatus.INFO, sourceElementDesc +" has been moved.");
		}
	}
	
	public static void moveToElement(WebElement element,LinkedHashMap<String,Object> TestInfo) throws IOException, InterruptedException
	{
		WebDriver driver = (WebDriver) (TestInfo.get("driver"));
		Actions action = new Actions(driver);
    	action.moveToElement(element).perform();
	}
	
	
//	public static String getAttribute(WebElement element,String attribute, LinkedHashMap<String,Object> TestInfo, String ElementDesc, String screenshot)
//	{
//		WebDriver driver = (WebDriver) (TestInfo.get("driver"));
//		try
//		{
//			element.getAttribute(attribute);
//		}
//		catch(Exception e)
//		{
//			e.printStackTrace();
////			org.testng.Assert.fail("fail");
////			org.testng.Assert.fail();
//			Assert.fail();
//		}
//		return element.getAttribute(attribute);
//	}
}
