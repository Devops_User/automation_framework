package generic;

import java.io.IOException;
import java.lang.reflect.Method;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;

//import org.openqa.selenium.PageLoadStrategy;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.ie.InternetExplorerDriverEngine;
//import org.openqa.selenium.ie.InternetExplorerOptions;
//import org.openqa.selenium.ie.InternetExplorerOptions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Listeners;
import org.testng.xml.XmlClass;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.ios.IOSDriver;

@Listeners(Listener.class)
public class Base
{
	public static LinkedHashMap<String,Object> SuiteTests = new LinkedHashMap<String,Object>();
	public static String ExecutionMode = null;
	public static String Instances = null;
	public static String TestList = "TestList";
	public static String ProjectPath = System.getProperty("user.dir");
	public static String MasterFilePath = ProjectPath + "//src//test//resources//data//MasterSheet.xlsx";
//	public static String ImagesPath = ProjectPath + "\\images\\";
	public static String ElementsDescription = ProjectPath + "//src//test//resources//elements//elements_description.properties";
	public static String ChromeExePath = "C:\\Users\\ashashi\\AppData\\Local\\Google\\Chrome\\Application\\chrome.exe";//ChromeOptions
	String NewLine = System.getProperty("line.separator");
	
	@BeforeSuite(alwaysRun = true) //******Load tests details, set exe paths, create log file******//
	public void LoadTests(ITestContext currentTestContext) throws IOException
	{	
		List<XmlClass> testClassNames = currentTestContext.getCurrentXmlTest().getClasses();
		if(testClassNames.contains("Invoker")==false)
		{
//			System.setProperty("webdriver.ie.driver", ProjectPath + "\\executables\\IEDriverServer_32.exe");
//			System.setProperty("webdriver.gecko.driver", ProjectPath + "\\executables\\geckodriver.exe");
			System.setProperty("webdriver.chrome.driver", ProjectPath + "//src//main//resources//executables//chromedriver");
			ExecutionMode = Utility.ReadCell(MasterFilePath,"ExecutionMode","ExecutionMode",1);
			Instances = Utility.ReadCell(MasterFilePath,"ExecutionMode","Instances",1);		
			int rows = Utility.GetRowCount(MasterFilePath, TestList);//**Get the count of rows with values from master sheet**//
			for(int i = 1; i <= rows-1 ; i++)//******For each test entry in the master sheet fetch the details******//
			{
				String Execute = Utility.ReadCell(MasterFilePath,TestList,"Execute",i);
				if(Execute.equalsIgnoreCase("yes"))//******If the test needs to be executed, add the details into global SuiteTests******//
				{
					String TestName = Utility.ReadCell(MasterFilePath,TestList,"TestName",i);
					String PropertyFilePath = ProjectPath + "//src//test//resources//config//" + TestName +".properties";
					String DataFilePath = ProjectPath + "//src//test//resources//data//" + TestName +".xlsx";
					//***Create object per test to be executed***//
					LinkedHashMap<String,Object> TestInfo = new LinkedHashMap<String,Object>();
					//***Load test details into the created object***//
					TestInfo.put("TestName", TestName);
					TestInfo.put("PropertyFilePath", PropertyFilePath);
					TestInfo.put("DataFilePath", DataFilePath);
					TestInfo.put("Type", Utility.ReadCell(MasterFilePath,TestList,"Type",i));
					TestInfo.put("Browser", Utility.ReadCell(MasterFilePath,TestList,"Browser",i));
					TestInfo.put("ExecuteRows", Utility.ReadCell(MasterFilePath,TestList,"ExecuteRows",i));
					TestInfo.put("TestDescription", Utility.ReadCell(MasterFilePath,TestList,"TestDescription",i));
					//***Following are Appium capabilities***//
					TestInfo.put("capabilities.automationName", Utility.ReadCell(MasterFilePath,TestList,"AutomationName",i));
					TestInfo.put("capabilities.platformName", Utility.ReadCell(MasterFilePath,TestList,"PlatformName",i));
					TestInfo.put("capabilities.deviceName", Utility.ReadCell(MasterFilePath,TestList,"DeviceName",i));
					TestInfo.put("capabilities.platformVersion", Utility.ReadCell(MasterFilePath,TestList,"PlatformVersion",i));
					TestInfo.put("capabilities.browserName", Utility.ReadCell(MasterFilePath,TestList,"BrowserName",i));
					TestInfo.put("capabilities.appPackage", Utility.ReadCell(MasterFilePath,TestList,"AppPackage",i));
					TestInfo.put("capabilities.appActivity", Utility.ReadCell(MasterFilePath,TestList,"AppActivity",i));
					//***Load the testname and the testinfo object that contains all test related information***//
					SuiteTests.put(TestName, TestInfo);
				}
			}
		}
	}	

	@DataProvider()
	public Object[][] GetExecutionRow(Method m) throws IOException
	{
		Object[][] dataEmpty = new Object[1][2];
		dataEmpty[0][0]= 0;
		dataEmpty[0][1]= "Empty";
		
		String TestName = m.getName();
		Object[][] data;
		if(SuiteTests.containsKey(TestName))
		{
			LinkedHashMap<String,Object> TestInfo= (LinkedHashMap<String, Object>) SuiteTests.get(TestName);//Get the testinfo object from using testname
			
			String DataFilePath = (String) TestInfo.get("DataFilePath");
			String ExecuteRows = (String) TestInfo.get("ExecuteRows");//***Get the rows to be executed: can be "all" or "specific row"***//
			LinkedHashMap<Integer,String> Rows = new LinkedHashMap<Integer,String>();
			TestInfo.put("Rows", Rows);//***Create LinkedHashMap to hold all the rows to be executed***//
			int row = 0;
			if(ExecuteRows.equalsIgnoreCase("all"))//***For "all" put all the row numbers into the LinkedHashMap***//
			{
				int rows = Utility.GetRowCount(DataFilePath, TestName);
				for(row = 1; row<=rows;row++)
				{
					Rows.put(row,"NotStarted");
				}
			}
			else //***Else put only the row number mentioned into the LinkedHashMap***//
			{
				Rows.put(Integer.parseInt(ExecuteRows), "NotStarted");
			}

			data = new Object[Rows.size()][2];
			int i = 0;
			for(Integer ExecuteRow:Rows.keySet())
			{
				data[i][0]= ExecuteRow;
				data[i][1]= Rows.get(ExecuteRow);
				i=i+1;
			}
			return data;
		}
		else
		{
			return dataEmpty;
		}
	}
	
	@BeforeMethod(alwaysRun = true)
	public void StartTest(Method m) throws IOException //***Report initialization, loading of rows to be executed***//
	{
		String TestName = m.getName();
		if (TestName.equalsIgnoreCase("Invoker"))//***Ignore for now***//
		{
			
		}
		else
		{
			if(SuiteTests.containsKey(TestName))//***If the test is in the suite test list***//
			{
				LinkedHashMap<String,Object> TestInfo=(LinkedHashMap<String, Object>) SuiteTests.get(TestName);
				if(TestInfo.containsKey("Initialization")==false)
				{
					WebDriver driver = AssignDriver(TestInfo);//***Based on browser and type of test, create the driver instance and return***//
					TestInfo.put("driver", driver);
					
					//***Report initialization***//
					String TimeStamp = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(new Date());
					String TestReportName = TestName + "_" + TimeStamp;
					String ReportPath = ProjectPath + "//src//test//resources//reports//" + TestReportName +".html";
					ExtentReports Extent = new ExtentReports(ReportPath,true);
					TestInfo.put("ReportPath", ReportPath);
					TestInfo.put("ExtentReport", Extent);
					TestInfo.put("Initialization", "DONE");
				}
			}
		}
	}
	
	public static WebDriver AssignDriver(LinkedHashMap<String,Object> TestInfo) throws IOException
	{
		WebDriver driver = null;
		String Browser = (String) TestInfo.get("Browser");
		String Type = (String) TestInfo.get("Type");
		String Key = null;		
		if (Type.equalsIgnoreCase("Web"))//***For "Web" create the driver instance***//
		{
			if (ExecutionMode.equalsIgnoreCase("Remote"))//***For remote execution create RemoteWebDriver instance***//
			{
				String node = "http://53.244.255.239:5568/wd/hub";
				DesiredCapabilities capability = new DesiredCapabilities();
				if(Browser.equalsIgnoreCase("chrome"))
				{
					capability.setBrowserName("chrome");
				}
				else if(Browser.equalsIgnoreCase("firefox"))
				{
					capability.setBrowserName("firefox");
				}
				else if(Browser.equalsIgnoreCase("internetexplorer"))
				{
					capability.setBrowserName("internetexplorer");
//					InternetExplorerOptions options = new InternetExplorerOptions();
//					options.setPageLoadStrategy(PageLoadStrategy.EAGER);

//					capability.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS, true);
//					capability.setCapability(InternetExplorerDriver.NATIVE_EVENTS, false);
				}
				driver = new RemoteWebDriver(new URL(node),capability);
			}
			else
			{
				if(Browser.equalsIgnoreCase("chrome"))//***For "chrome" create ChromeDriver instance***//
				{
//					ChromeOptions options = new ChromeOptions();
//					options.addArguments("--start-maximized");
////					options.addArguments("--headless");
//					options.setBinary(ChromeExePath);
//					
//					Map<String, Object> prefs = new HashMap<String, Object>();
//					prefs.put("credentials_enable_service", false);
//					prefs.put("profile.password_manager_enabled", false);
//					options.setExperimentalOption("prefs", prefs);
//
//					DesiredCapabilities capabilities = DesiredCapabilities.chrome();
//					capabilities.setCapability(ChromeOptions.CAPABILITY, options);
					driver = new ChromeDriver();
					
				}
				else if(Browser.equalsIgnoreCase("firefox"))//***For "firefox" create FirefoxDriver instance***//
				{
					FirefoxProfile profile=new FirefoxProfile();
					//if file is .zip never display popup download it directly
					String key="browser.helperApps.neverAsk.saveToDisk";
					String value="application/zip";
					profile.setPreference(key,value);
					//open firefox with above setting
//					driver = new FirefoxDriver(profile);
				}
				else if(Browser.equalsIgnoreCase("internetexplorer"))//***For "internetexplorer" create InternetExplorerDriver instance***//
				{
//					InternetExplorerOptions options = new InternetExplorerOptions().requireWindowFocus();
//					options.enablePersistentHovering();
//					options.introduceFlakinessByIgnoringSecurityDomains();
//					options.destructivelyEnsureCleanSession();
					DesiredCapabilities capability = new DesiredCapabilities();
//					capability.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS, true);
					capability.setCapability(InternetExplorerDriver.NATIVE_EVENTS, false);
					capability.setCapability(InternetExplorerDriver.IE_ENSURE_CLEAN_SESSION, true);
					driver = new InternetExplorerDriver(capability);
				}
			}
		}
		else if(Type.equalsIgnoreCase("Mobile"))//***For "Mobile" create the driver instance***//
		{
			DesiredCapabilities capabilities = new DesiredCapabilities();
			Set<String> keys = TestInfo.keySet();
			for(String key:keys)
			{
				if(key.contains("capabilities"))
				{
					String[] cap = key.split("\\.");
					Key=(String) TestInfo.get(key);
					if(Key.equalsIgnoreCase("na"))
					{
						
					}
					else
					{
						capabilities.setCapability(cap[1], TestInfo.get(key));
					}
				}
			}
			int x = 1;
			x=x+1;
			driver = new AndroidDriver(new URL("http://0.0.0.0:4723/wd/hub"), capabilities);
//			driver = new IOSDriver(new URL("http://0.0.0.0:4723/wd/hub"), capabilities);
		}
		driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
		try
		{
			driver.manage().window().maximize();	
		}
		catch(Exception e)
		{
			
		}
		return driver;
	}

	@AfterMethod(alwaysRun = true)
	public void EndTest(Method m,ITestResult Result) throws IOException
	{
		String TestName = m.getName();
		if(TestName.equalsIgnoreCase("InvokerTest"))
		{
			
		}
		else
		{
			if(SuiteTests.containsKey(TestName))
			{
				LinkedHashMap<String,Object> TestInfo = (LinkedHashMap<String, Object>) SuiteTests.get(TestName);
				LinkedHashMap<Integer,String> Rows = (LinkedHashMap<Integer, String>) TestInfo.get("Rows");
				int ExecuteRow = (Integer) TestInfo.get("ExecuteRow");
				Rows.put(ExecuteRow, "Executed");
				if(Rows.values().contains("NotStarted")==false)
				{
					String ReportPath = (String) TestInfo.get("ReportPath");
					String DataFilePath = (String) TestInfo.get("DataFilePath");				
					WebDriver driver = (WebDriver) TestInfo.get("driver");
					ExtentReports ExtentReport = (ExtentReports) TestInfo.get("ExtentReport");
					ExtentTest ExtentTest = (ExtentTest) TestInfo.get("ExtentTest");
					try
					{
						driver.close();
					}
					catch(Exception e)
					{
						
					}
					driver.quit();
					ExtentReport.endTest(ExtentTest);
					ExtentReport.flush();
					ExtentReport.endTest(null);
				}
			}
		}
	}
}