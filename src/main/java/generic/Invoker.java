package generic;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.testng.TestListenerAdapter;
import org.testng.TestNG;
import org.testng.annotations.Test;
import org.testng.xml.XmlClass;
import org.testng.xml.XmlSuite;
import org.testng.xml.XmlTest;
import generic.Base;

public class Invoker extends Base
{
	@Test
	public void InvokerTest() throws InterruptedException, IOException
	{
		int Threads = Integer.parseInt(Instances);
		XmlSuite suite = new XmlSuite();
		suite.setName("Suite");
		if(ExecutionMode.equalsIgnoreCase("Parallel"))
		{	
			suite.setParallel("methods");
			suite.setThreadCount(Threads);
		}		
		XmlTest test = new XmlTest(suite);
		test.setName("Test");
		List<XmlClass> classes = new ArrayList<XmlClass>();
		File[] files = new File(ProjectPath+"//src//test//java//tests//").listFiles();
		for (File file : files) 
		{
		    if (file.getName().contains("java")==true && file.getName().contains("Invoker")==false) 
		    {
			    	String[] fileName = file.getName().split("java");
			    	fileName[0] = fileName[0].replace(".", "");
			    	classes.add(new XmlClass("tests."+fileName[0]));
		    }
		}
		test.setXmlClasses(classes) ;
		TestListenerAdapter tla = new TestListenerAdapter();			
		List<XmlSuite> suites = new ArrayList<XmlSuite>();
		suites.add(suite);
		TestNG testng = new TestNG();
		testng.setXmlSuites(suites);
		testng.addListener(tla);
		testng.run(); 
	}
}