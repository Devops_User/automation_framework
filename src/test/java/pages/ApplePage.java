package pages;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.LinkedHashMap;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import generic.DriverActions;
import generic.Utility;

public class ApplePage 
{

	private WebDriver driver;
	private LinkedHashMap<String,Object> TestInfo;
	
//	@FindBy(id = "twotabsearchtextbox")
//	WebElement SearchField;

//	@FindBy(id = "nav-search-submit-text")
//	WebElement SearchButton;
	
//	@FindBy(id = "nav-search-submit-text")
//	WebElement SearchButton;
	
	@FindBy(xpath="//span[contains(text(),'Watch')]/..")       //form[@id='login-form']//input[@value='Log in']")
	WebElement WatchLink;
	
    public ApplePage(LinkedHashMap<String,Object> TestInfo)
    {
    	this.TestInfo = TestInfo;
    	this.driver = (WebDriver) TestInfo.get("driver");
        PageFactory.initElements(driver, this);
    }
    
    public void Search() throws FileNotFoundException, IOException, InterruptedException
    {
//	    	String SearchText = Utility.getValue("SearchText",TestInfo);
//	    	String SearchLinkText = Utility.getValue("SearchLinkText",TestInfo);
//	    	String SearchLinkText ="iPhoneX";
	    	
//	    	DriverActions.sendKeys(SearchField, SearchText, TestInfo, Utility.getElementDescription("AmazonPage.SearchField"), "0");
	    	DriverActions.click(WatchLink, TestInfo,Utility.getElementDescription("ApplePage.WatchLink"),"1");
	    	Thread.sleep(2000);
//	    	WebElement Link = driver.findElement(By.xpath("//a[contains(text(),'" + SearchLinkText + "')]"));
//	    	
//	    	DriverActions.isDisplayed(Link, TestInfo, SearchLinkText + " link", "1");
//	    	DriverActions.click(Link, TestInfo,SearchLinkText + " link","1");
    }
}