package pages;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.LinkedHashMap;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import generic.DriverActions;
import generic.Utility;

public class GooglePage 
{

	private WebDriver driver;
	private LinkedHashMap<String,Object> TestInfo;
	
	@FindBy(xpath = "//input[@name='q']")
	WebElement SearchField;

	@FindBy(xpath = "//button[@aria-label='Google Search']")
	WebElement SearchButton;
	
	@FindBy(xpath = "//a[@title='Google apps']")
	WebElement GoogleAppsMenu;
	
	@FindBy(xpath = "//span[text()='Play']")
	WebElement PlayButton;
	
//	@FindBy(xpath = "//span[text()='Play']")
//	WebElement PlaySearchField;
	
	@FindBy(xpath = "//a[text()='Books']")
	WebElement BooksTab;
	
	@FindBy(xpath = "//a[text()='Images']")
	WebElement ImagesTab;
	
	@FindBy(xpath = "//a[text()='News']")
	WebElement NewsTab;
	
	@FindBy(xpath = "//a[text()='Videos']")
	WebElement VideosTab;
	
    public GooglePage(LinkedHashMap<String,Object> TestInfo)
    {
    	this.TestInfo = TestInfo;
    	this.driver = (WebDriver) TestInfo.get("driver");
        PageFactory.initElements(driver, this);
    }
    
    public void Search() throws FileNotFoundException, IOException, InterruptedException
    {
	    	String SearchText = Utility.getValue("SearchText",TestInfo);
//	    	String SearchLinkText = Utility.getValue("SearchLinkText",TestInfo);
	    	String SearchLinkText ="Selenium";
	    	
	    	DriverActions.sendKeys(SearchField, SearchText, TestInfo, Utility.getElementDescription("GooglePage.SearchField"), "0");
	    	Thread.sleep(2000);
	    	SearchField.sendKeys(Keys.RETURN);
//	    	DriverActions.click(SearchButton, TestInfo,Utility.getElementDescription("GooglePage.SearchButton"),"1");
	    	Thread.sleep(2000);
	    	DriverActions.click(BooksTab, TestInfo,Utility.getElementDescription("GooglePage.BooksTab"),"1");
	    	DriverActions.click(ImagesTab, TestInfo,Utility.getElementDescription("GooglePage.ImagesTab"),"1");
	    	DriverActions.click(NewsTab, TestInfo,Utility.getElementDescription("GooglePage.NewsTab"),"1");
	    	DriverActions.click(VideosTab, TestInfo,Utility.getElementDescription("GooglePage.VideosTab"),"1");
    }
    
    
    public void Play() throws FileNotFoundException, IOException, InterruptedException
    {   	
	    	DriverActions.click(GoogleAppsMenu, TestInfo,Utility.getElementDescription("GooglePage.GoogleAppsMenu"),"1");
	    	Thread.sleep(2000);
	    	DriverActions.click(PlayButton, TestInfo,Utility.getElementDescription("GooglePage.PlayButton"),"1");
	    	Thread.sleep(2000);
	    	
	    	DriverActions.sendKeys(SearchField, "Google", TestInfo, Utility.getElementDescription("GooglePage.SearchField"), "0");
	    	Thread.sleep(2000);
	    	DriverActions.click(SearchButton, TestInfo,Utility.getElementDescription("GooglePage.SearchButton"),"1");
	    	Thread.sleep(2000);
    }
    
    

//	WebElement Link = driver.findElement(By.xpath("//a[contains(text(),'" + SearchLinkText + "')]"));
//	
//	DriverActions.isDisplayed(Link, TestInfo, SearchLinkText + " link", "1");
//	DriverActions.click(Link, TestInfo,SearchLinkText + " link","1");
}