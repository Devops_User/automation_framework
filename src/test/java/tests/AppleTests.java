package tests;
//package
import org.testng.annotations.Test;
import org.testng.annotations.Test;
import java.awt.AWTException;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.lang.reflect.Method;
import java.util.LinkedHashMap;
import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import generic.Base;
import generic.DriverActions;
import generic.Utility;
import pages.ApplePage;

public class AppleTests extends Base
{

	
	
	@Test(dataProvider="GetExecutionRow")
	public void AppleTestcase1(Method m, int ExecuteRow, String ExecuteStatus) throws FileNotFoundException, IOException, InterruptedException, AWTException
	{
		String TestName = m.getName();
		if(SuiteTests.containsKey(TestName))
		{
			LinkedHashMap<String,Object> TestInfo= (LinkedHashMap<String, Object>) SuiteTests.get(TestName);//Get the testinfo object from using testname
			LinkedHashMap<Integer,Integer> Rows = (LinkedHashMap<Integer, Integer>) TestInfo.get("Rows");
			ExtentReports Extent = (ExtentReports) TestInfo.get("ExtentReport");
			String TestDescription = (String) TestInfo.get("TestDescription");
	
			TestInfo.put("ExecuteRow",ExecuteRow);
			ExtentTest ExtentTest = Extent.startTest(TestName+" -> Data Row Number : "+ExecuteRow,TestDescription);
			TestInfo.put("ExtentTest", ExtentTest);

			String GTAppUrl = Utility.getValue("Url",TestInfo);
			DriverActions.navigate(TestInfo, GTAppUrl,"1");
			
			ApplePage applePage = new ApplePage(TestInfo);
			applePage.Search();		
		}
	}
}