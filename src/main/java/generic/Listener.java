package generic;

import java.io.IOException;
import java.util.LinkedHashMap;
import org.openqa.selenium.WebDriver;
import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

public class Listener implements ITestListener
{
	public void onTestStart(ITestResult result) 
	{
		
	}

	public void onTestSuccess(ITestResult result) 
	{
		
	}

	public void onTestFailure(ITestResult result) 
	{
		String path = null;
		LinkedHashMap<String,Object> TestInfo= (LinkedHashMap<String, Object>) Base.SuiteTests.get(result.getName());
		WebDriver driver = (WebDriver) TestInfo.get("driver");
		try 
		{
			path = Utility.captureScreenshot(driver);
		} 
		catch (IOException e) 
		{
			e.printStackTrace();
		}
		ExtentTest ExtentTest = (ExtentTest) TestInfo.get("ExtentTest");
		ExtentTest.log(LogStatus.FAIL, result.getThrowable().getMessage() + ExtentTest.addBase64ScreenShot(path));
	}

	public void onTestSkipped(ITestResult result) 
	{
		
	}

	public void onTestFailedButWithinSuccessPercentage(ITestResult result) 
	{
		
	}

	public void onStart(ITestContext context) 
	{
		
	}

	public void onFinish(ITestContext context) 
	{
		
	}
}
